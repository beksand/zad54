import static java.lang.Math.abs;

public class Main {

//    Napisz program obliczający całkowity pierwiastek kwadratowy z podanej liczby naturalnej. Nie korzystaj przy tym z
// funkcji bibliotecznych oraz innych metod przybliżonego obliczania pierwiastka kwadratowego i zaokrąglenia otrzymanego
// wyniku zmiennoprzecinkowego do liczby całkowitej.

    public static void main(String[] args) {
        System.out.println(mySqrt(81));


    }
    public static float mySqrt(int x){
        float S=x, a=1, b=x;
        while (abs(a-b)>0) {
            a=(a+b)/2; b = S / a;
        }
        return (a+b)/2;
    }

}
